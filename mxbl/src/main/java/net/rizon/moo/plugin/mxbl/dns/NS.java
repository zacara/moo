package net.rizon.moo.plugin.mxbl.dns;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Orillion <orillion@rizon.net>
 */
@Slf4j
public class NS
{
	/**
	 * Looks up the specified {@link RecordType}s for the given hostname.
	 * <p>
	 * @param hostname Hostname to look up.
	 * @param types    {@link RecordType}s to look up.
	 * <p>
	 * @return List of NameServer replies.
	 * <p>
	 */
	public static HashMap<RecordType, List<String>> lookup(String hostname, List<RecordType> types)
	{
		HashMap<RecordType, List<String>> m = new HashMap<RecordType, List<String>>();
		try
		{
			Hashtable env = new Hashtable();

			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");

			DirContext ictx = new InitialDirContext(env);

			for (RecordType record : types)
			{
				String r = record.toString();

				Attributes attrs = ictx.getAttributes(hostname, new String[]
				{
					r
				});

				List<String> records = processRecords(attrs.get(r));

				m.put(record, records);
			}

			return m;
		}
		catch (NamingException ex)
		{
			return null;
		}
		catch (UnknownHostException ex)
		{
			return null;
		}
	}

	private static List<String> processRecords(Attribute attr) throws UnknownHostException
	{
		ArrayList<String> list = new ArrayList<String>();
		if (attr == null)
		{
			return list;
		}
		for (int i = 0; i < attr.size(); i++)
		{
			try
			{
				Object o = attr.get(i);
				if (o instanceof String)
				{
					list.add((String) attr.get(i));
				}
			}
			catch (NamingException ex)
			{
				log.info("Naming Exception: ", ex.getMessage());
			}
		}
		return list;
	}
}
