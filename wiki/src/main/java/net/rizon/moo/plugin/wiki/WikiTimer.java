package net.rizon.moo.plugin.wiki;

import lombok.extern.slf4j.Slf4j;
import net.rizon.moo.Moo;
import net.rizon.moo.logging.LoggerUtils;

@Slf4j
class WikiTimer implements Runnable
{
	private WikiChecker c;
	
	@Override
	public void run()
	{
		if (c != null && c.isAlive())
			return;

		c = new WikiChecker();
		Moo.injector.injectMembers(c);
		LoggerUtils.initThread(log, c);
		c.start();
	}
}