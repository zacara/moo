package net.rizon.moo.protocol;

import lombok.extern.slf4j.Slf4j;
import net.rizon.moo.Message;
import net.rizon.moo.Moo;
import net.rizon.moo.io.IRCMessage;

@Slf4j
public class Message303 extends Message
{
	public Message303()
	{
		super("303");
	}

	@Override
	public void run(IRCMessage message)
	{
		for (final String nick : message.getParams()[1].split(" "))
			if (nick.equalsIgnoreCase("GeoServ"))
			{
				log.info("GeoServ has come back! Changing akillserv back to GeoServ");
				
				Moo.akillServ = "GeoServ";
			}
	}
}