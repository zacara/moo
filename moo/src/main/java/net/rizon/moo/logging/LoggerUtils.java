package net.rizon.moo.logging;

import org.slf4j.Logger;

public class LoggerUtils
{
	public static void initThread(final Logger logger, Thread t)
	{
		t.setUncaughtExceptionHandler((t1, e) -> logger.error("uncaught exception", e));
	}
}
