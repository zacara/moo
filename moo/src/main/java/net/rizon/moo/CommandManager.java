package net.rizon.moo;

import net.rizon.moo.conf.Config;
import net.rizon.moo.irc.User;
import com.google.inject.Inject;
import java.util.Set;
import net.rizon.moo.io.IRCMessage;
import net.rizon.moo.irc.IRC;
import net.rizon.moo.irc.Protocol;

public class CommandManager
{
	@Inject
	private Set<Command> commands;

	@Inject
	private IRC irc;

	@Inject
	private Protocol protocol;

	@Inject
	private Config config;
	
	private Command find(String name)
	{
		for (Command c : commands)
			if (c.getCommandName().equalsIgnoreCase(name))
				return c;
		return null;
	}
	
	public void run(IRCMessage m)
	{
		final String[] params = m.getParams();

		if (params.length < 2)
			return;

		final String target = params[0];
		final String message = params[1];

		if (!target.startsWith("#"))
			return;

		if (!message.startsWith("!")
			&& !message.startsWith(".")
			&& !message.startsWith(config.general.nick + ": ")
			&& !message.startsWith(config.general.nick + " "))
			return;

		final String[] tokens = message.split(" ");
		final String cmd = message.startsWith(config.general.nick) ? "!" + tokens[1] : tokens[0];

		Command c = find(cmd);
		if (c == null)
			return;

		if (!c.isRequiredChannel(target))
			return;

		User user = irc.findUser(m.getNick());
		if (user == null)
			user = new User(m.getNick());

		CommandSource csource = new CommandSource(protocol, user, irc.findChannel(target));
		c.execute(csource, tokens);
	}
}
