package net.rizon.moo.io;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InboundLoggingHandler extends SimpleChannelInboundHandler
{
	@Override
	public void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception
	{
		log.debug("<- {}", msg);
		
		ctx.fireChannelRead(msg);
	}
}
